import java.util.Scanner;

public class LeapYear {
    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);
        int year = userInput.nextInt();
        if(Year.isLeapYear(year)) {
            System.out.println(year + " is a leap year");
        } else {
            System.out.println(year + " is not a leap year");
        }
    }
}